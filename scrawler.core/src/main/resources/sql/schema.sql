CREATE TABLE `cimage` (
  `id int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(120) NOT NULL,
  `file_name` varchar(120) NOT NULL,
  `create_date` date DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `alt` varchar(120) DEFAULT NULL,
  `url_page` varchar(120) DEFAULT NOT NULL,
  `name_application` varchar(120) DEFAULT NOT NULL,
  `tag_classifier` varchar(120) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY uq_cimage_1 (`url`,`file_name`)
);
