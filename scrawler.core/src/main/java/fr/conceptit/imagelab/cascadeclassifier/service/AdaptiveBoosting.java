package fr.conceptit.imagelab.cascadeclassifier.service;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;


public interface AdaptiveBoosting {
	
	/**
	 * Objects Detect in a given image 
	 * @param cascadeFileName
	 * @param image
	 * @return
	 */
	MatOfRect detect(String cascadeFileName, Mat image, boolean flag, String color);
	
}
