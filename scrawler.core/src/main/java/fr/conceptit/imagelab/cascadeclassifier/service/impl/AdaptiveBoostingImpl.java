package fr.conceptit.imagelab.cascadeclassifier.service.impl;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import fr.conceptit.imagelab.cascadeclassifier.service.AdaptiveBoosting;
import fr.conceptit.imagelab.classify.AdaBoostClassifier;

@Component
public class AdaptiveBoostingImpl implements AdaptiveBoosting {

	final Logger logger = LoggerFactory.getLogger(AdaptiveBoostingImpl.class);
	
	@Override
	public MatOfRect detect(String cascadeFileName, Mat image, boolean flag, String color) {
		MatOfRect rects = new MatOfRect();
		AdaBoostClassifier adaBoost = new AdaBoostClassifier(cascadeFileName, image);
		adaBoost.setModelDetections(rects);
		adaBoost.detect(flag,color);
		return rects;
	}

}
