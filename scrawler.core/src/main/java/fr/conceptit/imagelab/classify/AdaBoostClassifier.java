package fr.conceptit.imagelab.classify;

import java.awt.Color;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.objdetect.CascadeClassifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.conceptit.imagelab.scrawler.Constants;

public class AdaBoostClassifier {

	final Logger logger = LoggerFactory.getLogger(AdaBoostClassifier.class);

	private Mat image = null;
	private CascadeClassifier cascadeClassifier = null;
	private MatOfRect modelDetections = null;
	private String cascadeClassifierFileName = null;

	/**
	 * Default constructor
	 */
	public AdaBoostClassifier(String xmlFileName) {
		try {
			setCascadeClassifier(new CascadeClassifier(Constants.DIR_CLASSIFIERS.concat(xmlFileName)));
		} catch (Exception e) {
			logger.error("Unable to instantiate an AdaBoost classifier. check the path to the file " + Constants.DIR_CLASSIFIERS.concat(xmlFileName));
		}
	}

	public AdaBoostClassifier(CascadeClassifier cascadeClassifier) {
		this.cascadeClassifier = cascadeClassifier;
	}

	public AdaBoostClassifier(String xmlFileName, String imageFileName) {
		try {

			setCascadeClassifier(new CascadeClassifier(Constants.DIR_CLASSIFIERS.concat(xmlFileName)));
			setImage(Constants.DIR_CLASSIFIERS.concat(imageFileName));

		} catch (Exception e) {
			logger.error("Unable to instantiate an AdaBoost classifier. Check the path of the files " + Constants.DIR_CLASSIFIERS.concat(xmlFileName) + " and "
					+ Constants.DIR_CLASSIFIERS.concat(imageFileName));
		}
	}

	public AdaBoostClassifier(String xmlFileName, Mat image) {
		try {
			setCascadeClassifierFileName(xmlFileName);
			setCascadeClassifier(new CascadeClassifier(Constants.DIR_CLASSIFIERS.concat(xmlFileName)));
			setImage(image);

		} catch (Exception e) {
			logger.error("A error is occured in image or xmlfilename " + Constants.DIR_CLASSIFIERS.concat(xmlFileName));
		}
	}

	public void detect(boolean flag, String color) {
		if (image != null && cascadeClassifier != null && modelDetections != null) {
			cascadeClassifier.detectMultiScale(image, modelDetections);
			if (flag)
				addRectToImage(color);
			logger.info("Detect " + modelDetections.toArray().length + " Object(s) using " + cascadeClassifierFileName);
		}
	}

	private void addRectToImage(String color) {
		
		Color rgbColor = Color.decode("#"+color);
		for (Rect rect : modelDetections.toArray()) {
			Core.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
					new Scalar(rgbColor.getBlue(), rgbColor.getGreen(), rgbColor.getRed()), 2);
		}
	}

	public void detect(Mat image, MatOfRect modelDetections, boolean flag, String color) {
		setModelDetections(modelDetections);
		setImage(image);
		if (cascadeClassifier != null)
			detect(flag, color);
	}

	public void setImage(String imageFileName) {
		try {
			setImage(Highgui.imread(Constants.DIR_IMAGES.concat(imageFileName)));
		} catch (Exception e) {
			logger.error("Unable to read image. Check the path to the file " + Constants.DIR_CLASSIFIERS.concat(imageFileName));
		}
	}

	public CascadeClassifier getCascadeClassifier() {
		return cascadeClassifier;
	}

	public void setCascadeClassifier(CascadeClassifier cascadeClassifier) {
		this.cascadeClassifier = cascadeClassifier;
	}

	public Mat getImage() {
		return image;
	}

	public MatOfRect getModelDetections() {
		return modelDetections;
	}

	public void setImage(Mat image) {
		this.image = image;
	}

	public void setModelDetections(MatOfRect modelDetections) {
		this.modelDetections = modelDetections;
	}

	public String getCascadeClassifierFileName() {
		return cascadeClassifierFileName;
	}

	public void setCascadeClassifierFileName(String cascadeClassifierFileName) {
		this.cascadeClassifierFileName = cascadeClassifierFileName;
	}

}
