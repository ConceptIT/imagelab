package fr.conceptit.imagelab.io;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.conceptit.imagelab.scrawler.Constants;

public class CImageReader {

	final Logger logger = LoggerFactory.getLogger(CImageReader.class);

	private String fileName;
	private String url;
	private String baseName;
	private String extension;
	private boolean exist;
	private BufferedImage image;
	private String md5;

	/**
	 * Default constructor
	 * 
	 * @param url
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public CImageReader(String url, boolean isFile) throws MalformedURLException, IOException {

		if (isFile)
			image = ImageIO.read(new File(url));
		else
			image = ImageIO.read(new URL(url));

		setUrl(url);
		setBaseName(FilenameUtils.getBaseName(url));
		setExtension(FilenameUtils.getExtension(url));
		setFileName(baseName.concat(".").concat(extension));
		setMd5(computeMd5());
		logger.info("Read image from " + url + " the file name is " + fileName + " and the MD5 is " + md5);
	}

	/**
	 * Compute MD5 for a given image
	 * 
	 * @return MD5
	 */
	private String computeMd5() {
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			ImageIO.write(image, "png", outputStream);
			byte[] data = outputStream.toByteArray();
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(data);
			byte[] hash = md.digest();
			return returnHex(hash);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	static String returnHex(byte[] inBytes) throws Exception {
		String hexString = "";
		for (int i = 0; i < inBytes.length; i++) { // for loop ID:1
			hexString += Integer.toString((inBytes[i] & 0xff) + 0x100, 16).substring(1);
		} // Belongs to for loop ID:1
		return hexString;
	}

	public Mat getImageAsMat() {
		logger.info("Convert BufferedImage to Mat OpenCV format ");
		byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
		mat.put(0, 0, data);
		return mat;
	}

	/**
	 * Save image to Constants.Dir_images. Default value is /opt/images/
	 */
	public void saveToDirImages() {
		try {
			ImageIO.write(image, extension, new File(Constants.DIR_IMAGES.concat(fileName)));
		} catch (IOException e) {
			logger.error("Impossible de sauver image:");
			return;
		}
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public boolean isExist() {
		return exist;
	}

	public void setExist(boolean exist) {
		this.exist = exist;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getBaseName() {
		return baseName;
	}

	public void setBaseName(String baseName) {
		this.baseName = baseName;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

}
