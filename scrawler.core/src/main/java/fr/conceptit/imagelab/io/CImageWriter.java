package fr.conceptit.imagelab.io;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.highgui.Highgui;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.conceptit.imagelab.scrawler.Constants;

public class CImageWriter {
	final Logger logger = LoggerFactory.getLogger(CImageWriter.class);
	private String fileName;
	private BufferedImage image;

	public CImageWriter(String fileName, BufferedImage image) {
		setFileName(fileName);
		setImage(image);
	}

	public CImageWriter(String fileName, Mat image) {
		setFileName(fileName);
		setImage(getBufferedImage(image));
	}

	public void save(String ext) {
		try {
			if (image == null || ext.isEmpty())
				throw new Exception("image is null or ext is empty");
			ImageIO.write(image, ext, new File(Constants.DIR_IMAGES.concat(fileName)));
		} catch (Exception e) {
			logger.error("Problem is occured during saving the image " + e.getMessage());
			return;
		}
	}

	private BufferedImage getBufferedImage(Mat img) {
		MatOfByte bytemat = new MatOfByte();
		Highgui.imencode(".jpg", img, bytemat);
		byte[] bytes = bytemat.toArray();
		InputStream in = new ByteArrayInputStream(bytes);
		BufferedImage im = null;
		try {
			im = ImageIO.read(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return im;
	}

	public String getFileName() {
		return fileName;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

}
