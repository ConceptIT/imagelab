package fr.conceptit.imagelab.scrawler.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "classifier")
public class Classifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7987328046652968371L;
	private Long id;
	private String fileName;
	private String tags;
	private String color;
	private Set<CImage> cimages = new HashSet<CImage>();
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "file_name")
	public String getFileName() {
		return fileName;
	}

	@Column(name = "tags")
	public String getTags() {
		return tags;
	}

	@Column(name = "color")
	public String getColor() {
		return color;
	}

	@ManyToMany
	@JoinTable(name = "cimage_verify_classifier", joinColumns = @JoinColumn (name="id_classifier"), inverseJoinColumns = @JoinColumn (name="id_cimage") )
	@JsonBackReference
	public Set<CImage> getCimages() {
		return cimages;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setCimages(Set<CImage> cimages) {
		this.cimages = cimages;
	}

}
