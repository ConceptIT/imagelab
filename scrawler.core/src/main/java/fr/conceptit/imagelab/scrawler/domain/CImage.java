package fr.conceptit.imagelab.scrawler.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
@Table(name = "cimage")
public class CImage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8671239336904874557L;

	// private Long id;
	private String url;
	private String alt;
	private DateTime createDate;
	private String description;
	private String fileName;
	private String nameApplication;
	private String urlPage;
	private String tagClassifier;
	private String md5;
	private Set<Classifier> classifiers = new HashSet<Classifier>();


	@Column(name = "url")
	public String getUrl() {
		return url;
	}

	@Column(name = "alt")
	public String getAlt() {
		return alt;
	}

	@Column(name = "create_date")
	@Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	@DateTimeFormat(iso = ISO.DATE)
	public DateTime getCreateDate() {
		return createDate;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	@Column(name = "file_name")
	public String getFileName() {
		return fileName;
	}

	@Id
	@Column(name = "md5")
	public String getMd5() {
		return md5;
	}

	@Column(name = "tag_classifier")
	public String getTagClassifier() {
		return tagClassifier;
	}

	@ManyToMany
	@JoinTable(name = "cimage_verify_classifier", joinColumns = @JoinColumn(name = "id_cimage"), inverseJoinColumns = @JoinColumn(name = "id_classifier"))
	@JsonManagedReference
	public Set<Classifier> getClassifiers() {
		return classifiers;
	}

	public void setClassifiers(Set<Classifier> classifiers) {
		this.classifiers = classifiers;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public void setCreateDate(DateTime createDate) {
		this.createDate = createDate;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setTagClassifier(String tagClassifier) {
		this.tagClassifier = tagClassifier;
	}

	@Column(name = "name_application")
	public String getNameApplication() {
		return nameApplication;
	}

	@Column(name = "url_page")
	public String getUrlPage() {
		return urlPage;
	}

	public void setNameApplication(String nameApplication) {
		this.nameApplication = nameApplication;
	}

	public void setUrlPage(String urlPage) {
		this.urlPage = urlPage;
	}

	@Transient
	public String getCreateDateString() {
		String createDateString = "";
		if (createDate != null)
			createDateString = org.joda.time.format.DateTimeFormat.forPattern("yyyy-MM-dd").print(createDate);
		return createDateString;
	}

}
