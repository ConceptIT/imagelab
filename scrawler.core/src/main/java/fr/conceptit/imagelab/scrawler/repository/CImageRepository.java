package fr.conceptit.imagelab.scrawler.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import fr.conceptit.imagelab.scrawler.domain.CImage;

public interface CImageRepository extends PagingAndSortingRepository<CImage, String> {

}
