package fr.conceptit.imagelab.scrawler.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import fr.conceptit.imagelab.scrawler.domain.Classifier;

public interface ClassifierRepository extends PagingAndSortingRepository<Classifier, Long> {
	
	Classifier findByFileName(String fileName);
	
}
