package fr.conceptit.imagelab.scrawler.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.conceptit.imagelab.scrawler.domain.CImage;

public interface CImageService {

	public List<CImage> findAll();

	public CImage findByMd5(String md5);
	
	public CImage save(CImage cimage);
	
	public Page<CImage> findAllByPage(Pageable pageable);
	
}
