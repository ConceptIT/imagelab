package fr.conceptit.imagelab.scrawler.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.conceptit.imagelab.scrawler.domain.CImage;
import fr.conceptit.imagelab.scrawler.domain.Classifier;


public interface ClassifierService  {

	public List<Classifier> findAll();

	public Classifier findById(Long id);
	
	public Classifier findByFileName(String fileName);
	
	public Classifier save(Classifier classifier);
	
	public Page<Classifier> findAllByPage(Pageable pageable);
	
}
