package fr.conceptit.imagelab.scrawler.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Set;

import org.joda.time.DateTime;
import org.json.JSONObject;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.conceptit.imagelab.cascadeclassifier.service.AdaptiveBoosting;
import fr.conceptit.imagelab.io.CImageReader;
import fr.conceptit.imagelab.io.CImageWriter;
import fr.conceptit.imagelab.scrawler.domain.CImage;
import fr.conceptit.imagelab.scrawler.domain.Classifier;
import fr.conceptit.imagelab.scrawler.service.CImageService;
import fr.conceptit.imagelab.scrawler.service.ClassifierService;
import fr.conceptit.imagelab.scrawler.service.SmartCrawler;

@Component
public class CrawlerGoogleImage implements SmartCrawler {

	final Logger logger = LoggerFactory.getLogger(CrawlerGoogleImage.class);

	private String query;
	private String url;
	private int responsePerPage;
	private Set<Classifier> classifiers = new HashSet<Classifier>();
	private int nbrMaxOfImages;

	@Autowired
	private CImageService cimageService;

	@Autowired
	private ClassifierService classifierService;

	@Autowired
	private AdaptiveBoosting adaBoost;

	public void setUrl(String url) {
		this.url = url;
		logger.info("Url = " + this.url);
	}

	public int getNbrMaxOfImages() {
		return nbrMaxOfImages;
	}

	public void setNbrMaxOfImages(int nbrMaxOfImages) {
		this.nbrMaxOfImages = nbrMaxOfImages;
	}

	public void setQuery(String query) {
		this.query = "&q=" + query;
		logger.info("Query = " + this.query);
	}

	public String getQuery() {
		return query;
	}

	public String getUrl() {
		return url;
	}

	public int getResponsePerPage() {
		return responsePerPage;
	}

	public void setResponsePerPage(int responsePerPage) {
		this.responsePerPage = responsePerPage;
	}

	public void setNameClassifiers(Set<String> nameClassifiers) {
		for (String name : nameClassifiers) {
			Classifier classifierInDataBase = classifierService.findById(Long.parseLong(name));
			if (classifierInDataBase != null)
				this.classifiers.add(classifierInDataBase);
		}
	}

	@Override
	public void crawler() {
		logger.info("\n Application => " + this.url + " Query " + this.query + " Nbr Max of Images is " + nbrMaxOfImages);

		int start = 0;
		while (start < nbrMaxOfImages) {
			URL ajaxUrl = null;
			try {

				ajaxUrl = new URL(url + query + "&start=" + start + "&rsz=" + responsePerPage);
				logger.info("\n\n\n" + ajaxUrl.toString() + "\n\n\n");

				URLConnection connection = ajaxUrl.openConnection();
				String line;
				StringBuilder builder = new StringBuilder();

				BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}

				JSONObject json = new JSONObject(builder.toString());

				for (int pos = 0; pos < responsePerPage; pos++) {

					String imageUrl = json.getJSONObject("responseData").getJSONArray("results").getJSONObject(pos).getString("url");
					CImageReader cimgReader = null;

					try {
						cimgReader = new CImageReader(imageUrl,false);

						if (cimgReader != null) {

							logger.debug("### Object CImage");
							CImage cimg = new CImage();
							cimg.setUrl(imageUrl);
							cimg.setCreateDate(new DateTime());
							cimg.setFileName(cimgReader.getFileName());
							cimg.setNameApplication("Google Image");
							cimg.setUrlPage(imageUrl);
							cimg.setMd5(cimgReader.getMd5());

							logger.debug("### Classification ");
							Mat imgData = cimgReader.getImageAsMat();
							boolean toPersist = false;
							for (Classifier classifier : classifiers) {

								MatOfRect modelDetections = adaBoost.detect(classifier.getFileName(), imgData, true, classifier.getColor());
								if (modelDetections.toArray().length != 0) {
									cimg.getClassifiers().add(classifier);
									cimg.setTagClassifier(classifier.getTags());
									toPersist = true;
								}

								if (toPersist) {
									logger.debug("### Persisantce DB");
									cimageService.save(cimg);

									logger.debug("### Persisantce FileSystem ");
									CImageWriter cimgWriter = new CImageWriter(cimgReader.getFileName(), imgData);
									cimgWriter.save(cimgReader.getExtension());
								}
							}
						}

					} catch (Exception e) {
						logger.error("Error in cimage " + imageUrl + "Error nature is " + e.getMessage());
						continue;
					}
				}

			} catch (Exception e) {
				start += 4;
				logger.error("Error in url  " + ajaxUrl + "Error nature is " + e.getMessage());
				continue;
			}
			start += 4;
		}
	}

}
