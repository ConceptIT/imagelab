package fr.conceptit.imagelab.scrawler.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.conceptit.imagelab.cascadeclassifier.service.AdaptiveBoosting;
import fr.conceptit.imagelab.io.CImageReader;
import fr.conceptit.imagelab.io.CImageWriter;
import fr.conceptit.imagelab.scrawler.domain.CImage;
import fr.conceptit.imagelab.scrawler.domain.Classifier;
import fr.conceptit.imagelab.scrawler.service.CImageService;
import fr.conceptit.imagelab.scrawler.service.ClassifierService;
import fr.conceptit.imagelab.scrawler.service.SmartCrawler;

@Component
public class SeleniumCrawlerWDBonCoin implements SmartCrawler {

	final Logger logger = LoggerFactory.getLogger(SeleniumCrawlerWDBonCoin.class);

	private String url;
	private String query;
	private WebDriver driver;
	private final static int pleaseWait = 10;
	private Set<Classifier> classifiers = new HashSet<Classifier>();
	private int nbrMaxOfImages;

	@Autowired
	private CImageService cimageService;

	@Autowired
	private ClassifierService classifierService;

	@Autowired
	private AdaptiveBoosting adaBoost;

	// Afin de ne pas activer javascript dans HtmlUnitDriver
	private String processingLink(String link) {
		String newStr1 = link.replace("background-image: url(", "");
		String newStr2 = newStr1.replace(");", "");
		String newStr3 = newStr2.replace("thumbs", "images");
		return newStr3;
	}

	private List<String> imagesLinksInAnnonce(String annonceLink) {

		driver.get(annonceLink);
		List<String> imagesLinks = new ArrayList<String>();

		// Current Image
		WebElement currentImage = driver.findElement(By.cssSelector("#image"));
		if (currentImage != null) {
			String currentImageStr = currentImage.getAttribute("style");
			imagesLinks.add(processingLink(currentImageStr));
		}

		// Others images
		List<WebElement> smallImages = driver.findElements(By.cssSelector(".thumbs"));
		if (!smallImages.isEmpty()) {
			for (WebElement image : smallImages) {
				String str;
				str = image.getAttribute("style");
				imagesLinks.add(processingLink(str));
			}
		}
		driver.manage().timeouts().implicitlyWait(pleaseWait, TimeUnit.SECONDS);
		return imagesLinks;
	}

	private List<String> linksInPage(String url) {
		List<String> links = new ArrayList<String>();
		driver.get(url);
		List<WebElement> wlinks = driver.findElements(By.cssSelector(".list-lbc>a"));
		if (wlinks == null)
			return links;
		for (WebElement link : wlinks) {
			String href = link.getAttribute("href");
			if (!href.isEmpty())
				links.add(href);
		}
		return links;
	}

	private void initSearchPage() {
		WebElement element = driver.findElement(By.name("q"));
		element.sendKeys(query);
		driver.findElement(By.xpath("//input[@id='searchbutton']")).click();
	}

	public String getUrl() {
		return url;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	@Override
	public void crawler() {
		logger.info("\n Application => " + url + "Classifier " + query);

		setDriver(new HtmlUnitDriver());
		driver.get(url);
		initSearchPage();

		// Pagination.
		List<String> availablePages = new ArrayList<String>();

		// first page 0
		availablePages.add(driver.getCurrentUrl());

		// Others pages
		List<WebElement> availableWEPages = driver.findElements(By.cssSelector("#paging>li>a"));
		if (availablePages != null)
			for (WebElement pageLink : availableWEPages)
				availablePages.add(pageLink.getAttribute("href"));

		// nbr de pages
		WebElement nbrWEPages = driver.findElement(By.xpath(".//*[@id='paging']/li[24]/a"));
		int nbrPages = 1;
		if (nbrWEPages != null)
			nbrPages = Integer.parseInt(nbrWEPages.getAttribute("href").split("o=")[1].split("&")[0]);
		logger.info("Nbr de pages est " + nbrPages);

		// ForEach Page
		int pageNumber = 0;
		for (String pageLink : availablePages) {
			// Get all annonceLink
			List<String> annoncesLinks = linksInPage(pageLink);
			for (String annonceLink : annoncesLinks) {
				List<String> images = imagesLinksInAnnonce(annonceLink);

				for (String imageUrl : images) {
					logger.info(imageUrl);
					CImageReader cimgReader = null;
					try {

						cimgReader = new CImageReader(imageUrl.replace("'",""), false);

						if (cimgReader != null) {

							logger.debug("### Object CImage");
							CImage cimg = new CImage();
							cimg.setUrl(cimgReader.getUrl());
							cimg.setCreateDate(new DateTime());
							cimg.setFileName(cimgReader.getFileName());
							cimg.setNameApplication("Bon coin");
							cimg.setUrlPage(annonceLink);
							cimg.setMd5(cimgReader.getMd5());

							logger.debug("### Classification ");
							Mat imgData = cimgReader.getImageAsMat();
							boolean toPersist = true;
							for (Classifier classifier : classifiers) {

								MatOfRect modelDetections = adaBoost.detect(classifier.getFileName(), imgData, true, classifier.getColor());
								if (modelDetections.toArray().length != 0) {
									cimg.getClassifiers().add(classifier);
									cimg.setTagClassifier(classifier.getTags());
									toPersist = true;
								}

								if (toPersist) {
									logger.debug("### Persisantce DB");
									cimageService.save(cimg);

									logger.debug("### Persisantce FileSystem ");
									CImageWriter cimgWriter = new CImageWriter(cimgReader.getFileName(), imgData);
									cimgWriter.save(cimgReader.getExtension());
								}
							}
						}

					} catch (Exception e) {
						logger.error("Error in cimage " + imageUrl + "Error nature is " + e.getMessage());
						continue;
					}

				}

			}
			if (pageNumber == 0)
				break;
			pageNumber++;
		}
		driver.close();
	}

	public Set<Classifier> getClassifiers() {
		return classifiers;
	}

	public void setClassifiers(Set<Classifier> classifiers) {
		this.classifiers = classifiers;
	}

	public void setNameClassifiers(Set<String> nameClassifiers) {
		for (String name : nameClassifiers) {
			Classifier classifierInDataBase = classifierService.findById(Long.parseLong(name));
			if (classifierInDataBase != null)
				this.classifiers.add(classifierInDataBase);
		}
	}

	public String getQuery() {
		return query;
	}

	public int getNbrMaxOfImages() {
		return nbrMaxOfImages;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public void setNbrMaxOfImages(int nbrMaxOfImages) {
		this.nbrMaxOfImages = nbrMaxOfImages;
	}

}
