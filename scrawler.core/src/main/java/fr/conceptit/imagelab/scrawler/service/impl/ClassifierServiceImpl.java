package fr.conceptit.imagelab.scrawler.service.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import fr.conceptit.imagelab.scrawler.domain.Classifier;
import fr.conceptit.imagelab.scrawler.repository.ClassifierRepository;
import fr.conceptit.imagelab.scrawler.service.ClassifierService;

@Service("classifierService")
@Repository
@Transactional
public class ClassifierServiceImpl implements ClassifierService {

	@Autowired
	private ClassifierRepository classifierRepository;

	@Transactional(readOnly = true)
	public List<Classifier> findAll() {
		return Lists.newArrayList(classifierRepository.findAll());
	}

	@Transactional(readOnly = true)
	public Classifier findById(Long id) {
		return classifierRepository.findOne(id);
	}

	public Classifier save(Classifier classifier) {
		return classifierRepository.save(classifier);
	}

	@Transactional(readOnly = true)
	public Page<Classifier> findAllByPage(Pageable pageable) {
		return classifierRepository.findAll(pageable);
	}

	@Override
	public Classifier findByFileName(String fileName) {
		return classifierRepository.findByFileName(fileName);
	}
	

}
