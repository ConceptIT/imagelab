package fr.conceptit.imagelab.scrawler;

public class Constants {

	/**
	 * The version of the application.
	 */
	public static final String VERSION = "0.0";

	/**
	 * US-style date format.
	 */
	public static final String DATEFORMAT_US = "MM/dd/yyyy";

	/**
	 * Shortened US-style date format.
	 */
	public static final String DATEFORMAT_US_SHORT = "MM/dd/yy";

	/**
	 * European-style date format.
	 */
	public static final String DATEFORMAT_FRENCH = "dd/MM/yyyy";

	/**
	 * Shortened European-style date format.
	 */
	public static final String DATEFORMAT_FRENCH_SHORT = "dd/MM/yy";

	/**
	 * Images Directory
	 */
	public static final String DIR_CLASSIFIERS = "/opt/classifiers/";

	/**
	 * Classifiers Directory
	 */
	public static final String DIR_IMAGES = "/opt/images/";

}
