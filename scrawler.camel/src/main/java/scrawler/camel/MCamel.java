package scrawler.camel;

import org.apache.camel.CamelContext;
import org.opencv.core.Core;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MCamel {

	public static void main(String[] args) throws Exception {
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:app-context.xml"); 
		CamelContext context = ctx.getBean("scrawlerContext", CamelContext.class);
		context.start();
		Thread.sleep(10000);
		context.stop();
	}

}
