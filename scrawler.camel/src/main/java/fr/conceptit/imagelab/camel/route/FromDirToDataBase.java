package fr.conceptit.imagelab.camel.route;

import javax.annotation.processing.Processor;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.joda.time.DateTime;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.conceptit.imagelab.cascadeclassifier.service.AdaptiveBoosting;
import fr.conceptit.imagelab.io.CImageReader;
import fr.conceptit.imagelab.io.CImageWriter;
import fr.conceptit.imagelab.scrawler.domain.CImage;
import fr.conceptit.imagelab.scrawler.service.CImageService;

public class FromDirToDataBase extends RouteBuilder {

	final Logger logger = LoggerFactory.getLogger(FromDirToDataBase.class);

	CImageReader cimgReader = null;

	@Autowired
	AdaptiveBoosting adaBoost;

	@Autowired
	private CImageService cimageService;

	@Override
	public void configure() throws Exception {

		from("file:/opt/images/tmp1?delete=true").process(new Processor() {

			@Override
			public void process(Exchange ex) throws Exception {

				logger.info(ex.getIn().getHeader("CamelFileName").toString());

				String imageUrl = new String(ex.getIn().getHeader("CamelFileAbsolutePath").toString());
				CImageReader cimgReader = new CImageReader(imageUrl, true);
				CImage cimg = new CImage();
				cimg.setUrl(imageUrl);
				cimg.setCreateDate(new DateTime());
				cimg.setFileName(cimgReader.getFileName());
				cimg.setNameApplication("OutWit Image");
				cimg.setUrlPage(imageUrl);
				cimg.setMd5(cimgReader.getMd5());

				Mat imgData = cimgReader.getImageAsMat();
				MatOfRect modelDetections = adaBoost.detect("lv.xml", imgData, true, "FFFF00");

				if (modelDetections.toArray().length != 0) {
					cimg.setTagClassifier("LV");
					// cimageService.save(cimg);
					CImageWriter cimgWriter = new CImageWriter(ex.getIn().getHeader("CamelFileAbsolutePath").toString(), imgData);
					cimgWriter.save(cimgReader.getExtension());
					logger.info(cimg.getFileName());
				}
			}
		}).to("file:/opt/images/tmp2");

	}

}
