package fr.conceptit.imagelab.scrawler.web.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;

import fr.conceptit.imagelab.scrawler.domain.CImage;
import fr.conceptit.imagelab.scrawler.service.CImageService;
import fr.conceptit.imagelab.scrawler.web.form.CImageGrid;
import fr.conceptit.imagelab.scrawler.web.form.Message;
import fr.conceptit.imagelab.scrawler.web.util.UrlUtil;

@RequestMapping("/cimages")
@Controller
public class CImageController {

	final Logger logger = LoggerFactory.getLogger(CImageController.class);

	@Autowired
	private CImageService cimageService;

	@Autowired
	MessageSource messageSource;

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		logger.info("Listing cimages");

		List<CImage> cimages = cimageService.findAll();
		uiModel.addAttribute("cimages", cimages);

		logger.info("No. of cimages: " + cimages.size());

		return "cimages/list";
	}

	@RequestMapping(value = "/{md5}", method = RequestMethod.GET)
	public String show(@PathVariable("md5") String md5, Model uiModel) {
		CImage cimage = cimageService.findByMd5(md5);
		uiModel.addAttribute("cimage", cimage);
		return "cimages/show";
	}

	// add new cimage
	@RequestMapping(params = "form", method = RequestMethod.POST)
	public String create(@Valid CImage cimage, BindingResult bindingResult,
			Model uiModel, HttpServletRequest httpServletRequest,
			RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Creating new cimage");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute(
					"message",
					new Message("error", messageSource.getMessage(
							"cimage_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("cimage", cimage);
			return "cimages/create";
		}

		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute(
				"message",
				new Message("success", messageSource.getMessage(
						"cimage_save_success", new Object[] {}, locale)));

		logger.info("cimage md5 : " + cimage.getMd5());

		cimageService.save(cimage);

		return "redirect:/cimages/"
				+ UrlUtil.encodeUrlPathSegment(cimage.getMd5(),
						httpServletRequest);
	}

	@RequestMapping(params = "form", method = RequestMethod.GET)
	public String createForm(Model uiModel) {
		CImage cimage = new CImage();
		uiModel.addAttribute("cimage", cimage);
		return "cimages/create";
	}

	// Update Form
	@RequestMapping(value = "/{md5}", params = "form", method = RequestMethod.POST)
	public String update(@Valid CImage cimage, BindingResult bindingResult,
			Model uiModel, HttpServletRequest httpServletRequest,
			RedirectAttributes redirectAttributes, Locale locale) {

		logger.info("Updating cimage");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute(
					"message",
					new Message("error", messageSource.getMessage(
							"cimage_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("cimage", cimage);
			return "cimages/update";
		}

		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute(
				"message",
				new Message("success", messageSource.getMessage(
						"cimage_save_success", new Object[] {}, locale)));
		cimageService.save(cimage);
		return "redirect:/cimages/"
				+ UrlUtil.encodeUrlPathSegment(cimage.getMd5(),
						httpServletRequest);
	}

	// Update Send
	@RequestMapping(value = "/{md5}", params = "form", method = RequestMethod.GET)
	public String updateForm(@PathVariable("md5") String md5, Model uiModel) {
		uiModel.addAttribute("cimage", cimageService.findByMd5(md5));
		return "cimages/update";
	}

	/**
	 * Support pagination for front-end grid
	 * @param uiModel
	 * @return
	 */
	@RequestMapping(value = "/listgrid", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public CImageGrid listGrid(
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "rows", required = false) Integer rows,
			@RequestParam(value = "sidx", required = false) String sortBy,
			@RequestParam(value = "sord", required = false) String order) {

		logger.info("Listing cimages for grid with page: {}, rows: {}", page,
				rows);
		logger.info("Listing cimages for grid with sort: {}, order: {}",
				sortBy, order);

		// Process order by
		Sort sort = null;
		String orderBy = sortBy;
		if (orderBy != null && orderBy.equals("createDateString"))
			orderBy = "createDate";

		if (orderBy != null && order != null) {
			if (order.equals("desc")) {
				sort = new Sort(Sort.Direction.DESC, orderBy);
			} else
				sort = new Sort(Sort.Direction.ASC, orderBy);
		}

		// Constructs page request for current page
		// Note: page number for Spring Data JPA starts with 0, while jqGrid
		// starts with 1
		PageRequest pageRequest = null;

		if (sort != null) {
			pageRequest = new PageRequest(page - 1, rows, sort);
		} else {
			pageRequest = new PageRequest(page - 1, rows);
		}

		Page<CImage> cimagePage = cimageService.findAllByPage(pageRequest);

		// Construct the grid data that will return as JSON data
		CImageGrid cimageGrid = new CImageGrid();
		cimageGrid.setCurrentPage(cimagePage.getNumber() + 1);
		cimageGrid.setTotalPages(cimagePage.getTotalPages());
		cimageGrid.setTotalRecords(cimagePage.getTotalElements());
		cimageGrid.setCimageData(Lists.newArrayList(cimagePage.iterator()));

		return cimageGrid;
	}

}
