package fr.conceptit.imagelab.scrawler.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HomeController {

	final Logger logger = LoggerFactory.getLogger(HomeController.class);

	// List
	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		logger.info("Page 0");
		return "home/home";
	}

}
