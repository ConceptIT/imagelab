package fr.conceptit.imagelab.scrawler.web.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.conceptit.imagelab.scrawler.domain.Classifier;
import fr.conceptit.imagelab.scrawler.service.ClassifierService;
import fr.conceptit.imagelab.scrawler.web.form.Message;
import fr.conceptit.imagelab.scrawler.web.util.UrlUtil;

@Controller
@RequestMapping("/classifiers")
public class ClassifierController {

	final Logger logger = LoggerFactory.getLogger(ClassifierController.class);

	@Autowired
	private ClassifierService classifierService;

	@Autowired
	MessageSource messageSource;

	// List
	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		logger.info("Listing classifiers");
		List<Classifier> classifiers = classifierService.findAll();
		uiModel.addAttribute("classifiers", classifiers);
		logger.info("No. of classifiers: " + classifiers.size());
		return "classifiers/list";
	}

	// add new classifier
	@RequestMapping(params = "form", method = RequestMethod.POST)
	public String create(@Valid Classifier classifier, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes,
			Locale locale) {
		logger.info("Creating new classifier");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message", new Message("error", messageSource.getMessage("classifier_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("classifier", classifier);
			return "classifiers/create";
		}
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message", new Message("success", messageSource.getMessage("classifier_save_success", new Object[] {}, locale)));
		logger.info("Classifier id : " + classifier.getId());
		classifierService.save(classifier);
		return "redirect:/classifiers/" + UrlUtil.encodeUrlPathSegment(classifier.getId().toString(), httpServletRequest);
	}

	@RequestMapping(params = "form", method = RequestMethod.GET)
	public String createForm(Model uiModel) {
		Classifier classifier = new Classifier();
		uiModel.addAttribute("classifier", classifier);
		return "classifiers/create";
	}

	// Show
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") Long id, Model uiModel) {
		Classifier classifier = classifierService.findById(id);
		uiModel.addAttribute("classifier", classifier);
		return "classifiers/show";
	}

	// Update Form
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.POST)
	public String update(@Valid Classifier classifier, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes,
			Locale locale) {
		logger.info("Updating classifier");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute("message", new Message("error", messageSource.getMessage("classifier_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("classifier", classifier);
			return "classifiers/update";
		}
		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute("message", new Message("success", messageSource.getMessage("classifier_save_success", new Object[] {}, locale)));
		classifierService.save(classifier);
		return "redirect:/classifiers/" + UrlUtil.encodeUrlPathSegment(classifier.getId().toString(), httpServletRequest);
	}

	// Update Send
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Long id, Model uiModel) {
		uiModel.addAttribute("classifier", classifierService.findById(id));
		return "classifiers/update";
	}

}
