
INSERT INTO application (id, url, name, version) VALUES
(1, 'http://www.leboncoin.fr/annonces/offres/ile_de_france/', 'bon coin ile de france', 0),
(2, 'http://www.leboncoin.fr/annonces/offres/bourgogne/', 'bon coin de bourgogne', 0);


INSERT INTO cimage (id, url, file_name, create_date, description, alt, image, version) VALUES
(1, 'urlimage1', 'image.jpg', '1980-07-30', NULL, NULL, NULL, 0),
(2, 'urlimage2', 'image2.ong', '1980-07-30', NULL, NULL, NULL, 0),
(3, 'urlimage3', 'image3.ong', '1980-07-30', NULL, NULL, NULL, 0),
(4, 'urlimage4', 'image4.ong', '1980-07-30', NULL, NULL, NULL, 0),
(5, 'urlimage5', 'image5.ong', '1980-07-30', NULL, NULL, NULL, 0),
(6, 'urlimage6', 'image6.ong', '1980-07-30', NULL, NULL, NULL, 0),
(7, 'urlimage7', 'image7.ong', '1980-07-30', NULL, NULL, NULL, 0),
(8, 'urlimage8', 'image8.ong', '1980-07-30', NULL, NULL, NULL, 0),
(9, 'urlimage9', 'image9.ong', '1980-07-30', NULL, NULL, NULL, 0);

INSERT INTO classifier (id, file_name, tags, version) VALUES
(1, 'cascade_lbp_adidas.xml', 'adidas', 0),
(2, 'cascade_haar_adidas.xml', 'adidas', 0),
(3, 'cascade_lbp_nike.xml', 'nike', 0),
(4, 'cascade_lbp_viuitton.xml', 'louis viuitton', 0),
(5, 'cascade_lbp_renault.xml', 'Renault', 0);


INSERT INTO page (id, id_app, url, title, version) VALUES
(1, 1, 'http://www.leboncoin.fr/vetements/611004460.htm?ca=12_s', 'Jean Guess Femme Authentique-26/32', 0);



INSERT INTO cimage_from_page (id_cimage, id_page) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1);


INSERT INTO cimage_verify_classifier (id_cimage, id_classifier) VALUES (1, 1),(1, 2),(1,3),(1, 4),(2, 1),(2, 2),(3, 4);


INSERT INTO crawler (id, name, status, version, description, create_date, nbr_of_images) VALUES
(1, 'test bon coin', 'terminated', 1, 'le premier test ..', '2014-01-29', 100),
(2, 'test bon coin', 'canceled', 1, 'le premier test ..', '2014-01-29', 1000);