CREATE TABLE application (
  id int(11) NOT NULL AUTO_INCREMENT,
  url varchar(120) NOT NULL,
  name varchar(40) NOT NULL,
  version int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (id),
  UNIQUE KEY uq_application_1 (url,name)
);

CREATE TABLE crawler (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(60) NOT NULL,
  status varchar(60) NOT NULL,
  version int(11) NOT NULL,
  description varchar(2000) NOT NULL,
  create_date date NOT NULL,
  nbr_of_images int(11) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE cimage (
  id int(11) NOT NULL AUTO_INCREMENT,
  url varchar(120) NOT NULL,
  file_name varchar(40) NOT NULL,
  create_date date DEFAULT NULL,
  description varchar(2000) DEFAULT NULL,
  alt varchar(120) DEFAULT NULL,
  image blob,
  version int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (id),
  UNIQUE KEY uq_cimage_1 (url,file_name)
);


CREATE TABLE classifier (
  id int(11) NOT NULL AUTO_INCREMENT,
  file_name varchar(120) NOT NULL,
  tags varchar(200) NOT NULL,
  version int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (id),
  UNIQUE KEY uq_classifier_1 (file_name)
);

CREATE TABLE page (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_app int(11) NOT NULL,
  url varchar(120) NOT NULL,
  title varchar(40) NOT NULL,
  version int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
  ,UNIQUE KEY uq_page_1 (url)
  ,CONSTRAINT FK_PAGE_APP_1 FOREIGN KEY (id_app) REFERENCES application (id) ON DELETE RESTRICT
);


CREATE TABLE cimage_from_page (
   id_cimage int(11) NOT NULL
  ,id_page int(11) NOT NULL
  ,PRIMARY KEY (id_cimage,id_page)
  ,CONSTRAINT FK_CIMAGE_PAGE_1 FOREIGN KEY (id_cimage) REFERENCES cimage (id) ON DELETE CASCADE
  ,CONSTRAINT FK_CIMAGE_PAGE_2 FOREIGN KEY (id_page) REFERENCES page (id) ON DELETE CASCADE 
);



CREATE TABLE cimage_verify_classifier (
  id_cimage int(11) NOT NULL
  ,id_classifier int(11) NOT NULL
  ,PRIMARY KEY (id_cimage,id_classifier)
  ,CONSTRAINT FK_CIMAGE_CLASSIFIER_1 FOREIGN KEY (id_cimage) REFERENCES cimage (id) ON DELETE CASCADE
  ,CONSTRAINT FK_CIMAGE_CLASSIFIER_2 FOREIGN KEY (id_classifier) REFERENCES classifier (id) ON DELETE CASCADE
);

