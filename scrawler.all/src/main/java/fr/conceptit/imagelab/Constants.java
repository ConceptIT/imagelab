package fr.conceptit.imagelab;

public class Constants {

	public static final String VERSION = "1.0";
	public static final String DIR_CLASSIFIERS = "/opt/classifiers/";
	public static final String DIR_IMAGES = "/opt/images/";
	
}
