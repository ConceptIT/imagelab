package fr.conceptit.imagelab.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "application")
public class Application implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1690511058735475471L;
	private Long id;
	private String name;
	private String url;
	private int version;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	
	@NotEmpty(message="{validation.name.NotEmpty.message}")
	@Column(name = "name")
	public String getName() {
		return name;
	}
	
	@NotEmpty(message="{validation.url.NotEmpty.message}")
	@Column(name = "url")
	public String getUrl() {
		return url;
	}
	
	
	@Version
	@Column(name = "version")
	public int getVersion() {
		return version;
	}


	public void setVersion(int version) {
		this.version = version;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	public String toString() {
		return "Application - Id: " + id + ", url: " + url + " Name :" + name;
	}
	
	
}
