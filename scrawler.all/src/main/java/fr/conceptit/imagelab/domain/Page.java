package fr.conceptit.imagelab.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table( name = "page")
public class Page implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3613327713192521504L;
	private Long id;
	private String title;
	private int version;
	private String url;
	private Application assignedApplication;
	private Set<CImage> cimages = new HashSet<CImage>();
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	
	@NotEmpty(message="{validation.title.NotEmpty.message}")
	@Column( name = "title")
	public String getTitle() {
		return title;
	}
	
	@Version
	@Column(name = "version")
	public int getVersion() {
		return version;
	}

	@NotEmpty(message="{validation.url.NotEmpty.message}")
	@Column( name = "url")
	public String getUrl() {
		return url;
	}

	@ManyToOne
	@JoinColumn(name="id_app")
	public Application getAssignedApplication() {
		return assignedApplication;
	}

	@ManyToMany
	@JoinTable(name = "cimage_from_page", joinColumns = @JoinColumn (name="id_cimage"), inverseJoinColumns = @JoinColumn (name="id_page") )
	public Set<CImage> getCimages() {
		return cimages;
	}

	public void setCimages(Set<CImage> cimages) {
		this.cimages = cimages;
	}

	public void setAssignedApplication(Application assignedApplication) {
		this.assignedApplication = assignedApplication;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Page [id=" + id + ", title=" + title + "]";
	}
	
	
}
