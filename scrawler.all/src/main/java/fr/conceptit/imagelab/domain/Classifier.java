package fr.conceptit.imagelab.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.hibernate.validator.constraints.NotEmpty;


@Entity
@Table (name = "classifier")
public class Classifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1232471506786518337L;
	private Long id;
	private String fileName;
	private String tags;
	private int version;
	private Set<CImage> cimages = new HashSet<CImage>();
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID")
	public Long getId() {
		return id;
	}
	
	
	@NotEmpty(message="{validation.filename.NotEmpty.message}")
	@Size(min=5, max=60, message="{validation.filename.Size.message}")
	@Column( name = "FILE_NAME")
	public String getFileName() {
		return fileName;
	}
	
	@NotEmpty(message="{validation.tags.NotEmpty.message}")
	@Size(min=10, max=120, message="{validation.tags.Size.message}")
	@Column( name = "TAGS")
	public String getTags() {
		return tags;
	}
	
	
	@Version
	@Column(name = "VERSION")
	public int getVersion() {
		return version;
	}

	@ManyToMany
	@JoinTable(name = "cimage_verify_classifier", joinColumns = @JoinColumn (name="id_classifier"), inverseJoinColumns = @JoinColumn (name="id_cimage") )
	@JsonBackReference
	public Set<CImage> getCimages() {
		return cimages;
	}

	public void setCimages(Set<CImage> cimages) {
		this.cimages = cimages;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "Classifier [id=" + id + ", fileName=" + fileName + ", tags="
				+ tags + "]";
	}
	
}
