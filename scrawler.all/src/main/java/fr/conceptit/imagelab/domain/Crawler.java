package fr.conceptit.imagelab.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import static javax.persistence.GenerationType.IDENTITY;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
@Table (name = "crawler")
public class Crawler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4256064886545295683L;
	private Long id;
	private String name;
	private String status;
	private int nbrOfImages;
	private int version;
	private DateTime createDate;
	private String description;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	
	@Column (name = "name")
	public String getName() {
		return name;
	}
	
	@Version
	@Column(name = "version")
	public int getVersion() {
		return version;
	}
	
	@Column(name = "create_date")
	@Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	@DateTimeFormat(iso = ISO.DATE)
	public DateTime getCreateDate() {
		return createDate;
	}
	
	
	@Transient
	public String getCreateDateString() {
		String birthDateString = "";
		if (createDate != null)
			birthDateString = org.joda.time.format.DateTimeFormat.forPattern(
					"yyyy-MM-dd").print(createDate);
		return birthDateString;
	}
	
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	@Column (name = "description")
	public String getDescription() {
		return description;
	}

	@Column (name = "nbr_of_images")
	public int getNbrOfImages() {
		return nbrOfImages;
	}

	public void setNbrOfImages(int nbrOfImages) {
		this.nbrOfImages = nbrOfImages;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public void setCreateDate(DateTime createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "Crawler [id=" + id + ", name=" + name + ", version=" + version
				+ ", createDate=" + createDate + "]";
	}
	
	
	
}
