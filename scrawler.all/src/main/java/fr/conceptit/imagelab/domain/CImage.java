package fr.conceptit.imagelab.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
@Table(name = "cimage")
public class CImage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6578581488382481089L;
	private Long id;
	private int version;
	private String url;
	private String alt;
	private DateTime createDate;
	private String description;
	private byte[] image;
	private String fileName;
	private Set<Page> pages = new HashSet<Page>();
	private Set<Classifier> classifiers = new HashSet<Classifier>();
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	@Version
	@Column(name = "version")
	public int getVersion() {
		return version;
	}

	@NotEmpty(message="{validation.filename.NotEmpty.message}")
	@Column(name = "file_name")
	public String getFileName() {
		return fileName;
	}
	
	@NotEmpty(message="{validation.url.NotEmpty.message}")
	@Column(name = "url")
	public String getUrl() {
		return url;
	}

	@Column(name = "alt")
	public String getAlt() {
		return alt;
	}

	@Column(name = "create_date")
	@Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
	@DateTimeFormat(iso = ISO.DATE)
	public DateTime getCreateDate() {
		return createDate;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	@Basic(fetch = FetchType.LAZY)
	@Lob
	@Column(name = "image")
	public byte[] getImage() {
		return image;
	}

	@ManyToMany
	@JoinTable(name = "cimage_from_page", joinColumns = @JoinColumn (name="id_page"), inverseJoinColumns = @JoinColumn (name="id_cimage") )
	@JsonIgnore
	public Set<Page> getPages() {
		return pages;
	}

	@ManyToMany
	@JoinTable(name = "cimage_verify_classifier", joinColumns = @JoinColumn (name="id_cimage"), inverseJoinColumns = @JoinColumn (name="id_classifier") )
	@JsonManagedReference
	public Set<Classifier> getClassifiers() {
		return classifiers;
	}

	public void setClassifiers(Set<Classifier> classifiers) {
		this.classifiers = classifiers;
	}

	public void setPages(Set<Page> pages) {
		this.pages = pages;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public void setCreateDate(DateTime createDate) {
		this.createDate = createDate;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}


	@Transient
	public String getCreateDateString() {
		String createDateString = "";
		if (createDate != null)
			createDateString = org.joda.time.format.DateTimeFormat.forPattern(
					"yyyy-MM-dd").print(createDate);
		return createDateString;
	}
	
	public String toString() {
		return "CImage - Id: " + id + ", url: " + url + " file name :" + fileName + ", alt: " + alt + ", create day: " + createDate
				+ ", Description: " + description;
	}

}
