package fr.conceptit.imagelab.repository;

import org.springframework.data.repository.CrudRepository;

import fr.conceptit.imagelab.domain.Crawler;

public interface CrawlerRepository extends CrudRepository<Crawler, Long> {

}
