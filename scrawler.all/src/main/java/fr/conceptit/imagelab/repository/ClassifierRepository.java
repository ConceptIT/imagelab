package fr.conceptit.imagelab.repository;

import org.springframework.data.repository.CrudRepository;

import fr.conceptit.imagelab.domain.Classifier;

public interface ClassifierRepository extends CrudRepository<Classifier, Long> {

}
