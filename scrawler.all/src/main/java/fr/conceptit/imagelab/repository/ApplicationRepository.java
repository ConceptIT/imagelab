package fr.conceptit.imagelab.repository;

import org.springframework.data.repository.CrudRepository;

import fr.conceptit.imagelab.domain.Application;

public interface ApplicationRepository extends
		CrudRepository<Application, Long> {

}
