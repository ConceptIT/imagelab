package fr.conceptit.imagelab.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import fr.conceptit.imagelab.domain.CImage;

public interface CImageRepository extends PagingAndSortingRepository<CImage, Long> {

}
