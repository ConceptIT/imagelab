package fr.conceptit.imagelab.repository;

import org.springframework.data.repository.CrudRepository;

import fr.conceptit.imagelab.domain.Page;

public interface PageRepository extends CrudRepository<Page, Long> {

}
