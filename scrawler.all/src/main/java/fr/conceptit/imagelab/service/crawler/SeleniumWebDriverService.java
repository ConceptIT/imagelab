package fr.conceptit.imagelab.service.crawler;

import fr.conceptit.imagelab.domain.Application;
import fr.conceptit.imagelab.domain.Classifier;

public interface SeleniumWebDriverService {
	
	void crawler (Application application, Classifier classifier);

}
