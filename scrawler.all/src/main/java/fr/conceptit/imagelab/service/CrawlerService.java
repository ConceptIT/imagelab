package fr.conceptit.imagelab.service;

import java.util.List;

import fr.conceptit.imagelab.domain.Crawler;

public interface CrawlerService {
	
	
	public List<Crawler> findAll();

	public Crawler findById(Long id);

	public Crawler save(Crawler crawler);

}
