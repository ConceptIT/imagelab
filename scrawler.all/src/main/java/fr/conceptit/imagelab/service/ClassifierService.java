package fr.conceptit.imagelab.service;

import java.util.List;

import fr.conceptit.imagelab.domain.Classifier;

public interface ClassifierService {

	
	public List<Classifier> findAll();

	public Classifier findById(Long id);

	public Classifier save(Classifier classifier);
	
}
