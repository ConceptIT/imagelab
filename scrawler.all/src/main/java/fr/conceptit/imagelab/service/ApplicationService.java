package fr.conceptit.imagelab.service;

import java.util.List;

import fr.conceptit.imagelab.domain.Application;

public interface ApplicationService {
	
	public List<Application> findAll();

	public Application findById(Long id);

	public Application save(Application application);
}
