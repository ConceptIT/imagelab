package fr.conceptit.imagelab.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import fr.conceptit.imagelab.domain.Application;
import fr.conceptit.imagelab.repository.ApplicationRepository;
import fr.conceptit.imagelab.service.ApplicationService;

@Service ("applicationService")
@Repository
@Transactional
public class ApplicationServiceImpl implements ApplicationService {

	@Autowired
	private ApplicationRepository applicationRepository;
	
	@Override
	public List<Application> findAll() {
		return Lists.newArrayList(applicationRepository.findAll());
	}

	@Override
	public Application findById(Long id) {
		return applicationRepository.findOne(id);
	}

	@Override
	public Application save(Application application) {
		return applicationRepository.save(application);
	}

}
