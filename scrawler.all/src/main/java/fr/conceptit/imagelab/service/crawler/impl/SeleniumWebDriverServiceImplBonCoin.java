package fr.conceptit.imagelab.service.crawler.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.util.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.conceptit.imagelab.domain.Application;
import fr.conceptit.imagelab.domain.CImage;
import fr.conceptit.imagelab.domain.Classifier;
import fr.conceptit.imagelab.repository.CImageRepository;
import fr.conceptit.imagelab.service.crawler.SeleniumWebDriverService;

@Service("crawlerSeleniumService")
@Repository
@Transactional
public class SeleniumWebDriverServiceImplBonCoin implements
		SeleniumWebDriverService {

	final Logger logger = LoggerFactory
			.getLogger(SeleniumWebDriverServiceImplBonCoin.class);

	private String url;
	private WebDriver driver;
	private String tag;

	@Autowired
	private CImageRepository cimageRepository;

	@Override
	public void crawler(Application application, Classifier classifier) {

		logger.info("Application => " + application.getUrl() + "Classifier "
				+ classifier.getFileName());

		setDriver(new HtmlUnitDriver());
		setUrl(application.getUrl());

		// En absence de classifier. L'adresse de crawler est l'url de
		// l'application.

		driver.get(url);
		if (classifier == null)
			setTag("None");
		else {
			setTag(classifier.getTags());
			initSearchPage();
		}

		// Pagination.
		List<WebElement> availableWEPages = driver.findElements(By
				.cssSelector("#paging>li>a"));
		List<String> availablePages = new ArrayList<String>();

		// page 0
		availablePages.add(driver.getCurrentUrl());

		// Tous les liens
		for (WebElement pageLink : availableWEPages) {
			availablePages.add(pageLink.getAttribute("href"));
		}

		// nbr de pages
		WebElement nbrWEPages = driver.findElement(By
				.xpath(".//*[@id='paging']/li[24]/a"));
//		int nbrPage = Integer.parseInt(nbrWEPages.getAttribute("href").split(
//				"o=")[1].split("&")[0]);

		// ForEach Page
		int pageNumber = 0;
		for (String pageLink : availablePages) {
			logger.info("Page " + pageLink);
			// Get all annonceLink
			List<String> annoncesLinks = linksInPage(pageLink);
			for (String annonceLink : annoncesLinks) {
				logger.info(">> Annonce " + annonceLink);
				List<String> images = imagesLinksInAnnonce(annonceLink);
				int pos = 0;
				for (String image : images) {
					logger.info("image -> " + image);
//					CImage cimg = new CImage();
//					cimg.setAlt("toto");
//					cimg.setFileName("iqqsdqsdqsdsmage"+pos+".jpeg");
//					cimg.setUrl(image);
//					cimageRepository.save(cimg);
					pos++;
				}
			}
			if (pageNumber == 0)
				break;
			pageNumber++;
		}
		driver.close();
	}

	// Afin de ne pas activer javascript dans HtmlUnitDriver
	private String processingLink(String link) {
		String newStr1 = link.replace("background-image: url(", "");
		String newStr2 = newStr1.replace(");", "");
		String newStr3 = newStr2.replace("thumbs", "images");
		return newStr3;
	}

	private List<String> imagesLinksInAnnonce(String annonceLink) {

		driver.get(annonceLink);
		List<String> imagesLinks = new ArrayList<String>();

		// Current Image
		WebElement currentImage = driver.findElement(By.cssSelector("#image"));
		String currentImageStr = currentImage.getAttribute("style");
		imagesLinks.add(processingLink(currentImageStr));
		System.out.println(">>> image " + processingLink(currentImageStr));

		// Others images
		List<WebElement> smallImages = driver.findElements(By
				.cssSelector(".thumbs"));
		for (WebElement image : smallImages) {
			String str;
			str = image.getAttribute("style");
			imagesLinks.add(processingLink(str));
			System.out.println(">>> image " + processingLink(str));
		}

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		return imagesLinks;
	}

	private List<String> linksInPage(String url) {

		driver.get(url);

		List<WebElement> wlinks = driver.findElements(By
				.cssSelector(".list-lbc>a"));

		List<String> links = new ArrayList<String>();
		for (WebElement link : wlinks) {
			links.add(link.getAttribute("href"));
		}
		return links;
	}

	private void initSearchPage() {
		WebElement element = driver.findElement(By.name("q"));
		element.sendKeys(tag);
		driver.findElement(By.xpath("//input[@id='searchbutton']")).click();
	}

	public String getUrl() {
		return url;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public String getTag() {
		return tag;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

}
