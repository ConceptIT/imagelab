package fr.conceptit.imagelab.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import fr.conceptit.imagelab.domain.Classifier;
import fr.conceptit.imagelab.repository.ClassifierRepository;
import fr.conceptit.imagelab.service.ClassifierService;

@Service ("classifierService")
@Repository
@Transactional
public class ClassifierServiceImpl implements ClassifierService {


	@Autowired
	private ClassifierRepository classifierRepository;
	
	@Override
	public List<Classifier> findAll() {
		return Lists.newArrayList(classifierRepository.findAll());
	}

	@Override
	public Classifier findById(Long id) {
		return classifierRepository.findOne(id);
	}

	@Override
	public Classifier save(Classifier classifier) {
		return classifierRepository.save(classifier);
				
	}

}
