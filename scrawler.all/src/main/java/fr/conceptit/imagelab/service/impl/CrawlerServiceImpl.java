package fr.conceptit.imagelab.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import fr.conceptit.imagelab.domain.Crawler;
import fr.conceptit.imagelab.repository.CrawlerRepository;
import fr.conceptit.imagelab.service.CrawlerService;

@Service ("crawlerService")
@Repository
@Transactional
public class CrawlerServiceImpl implements CrawlerService {

	@Autowired
	private CrawlerRepository crawlerRepository;
	
	@Override
	@Transactional(readOnly=true)
	public List<Crawler> findAll() {
		return Lists.newArrayList(crawlerRepository.findAll());
	}

	@Override
	@Transactional(readOnly=true)
	public Crawler findById(Long id) {
		return crawlerRepository.findOne(id);
	}

	@Override
	public Crawler save(Crawler crawler) {
		return crawlerRepository.save(crawler);
	}

}
