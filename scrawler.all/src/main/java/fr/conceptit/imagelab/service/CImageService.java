package fr.conceptit.imagelab.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.conceptit.imagelab.domain.CImage;

public interface CImageService {

	public List<CImage> findAll();

	public CImage findById(Long id);

	public CImage save(CImage cimage);
	
	public Page<CImage> findAllByPage(Pageable pageable);
	
}
