package fr.conceptit.imagelab.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import fr.conceptit.imagelab.domain.Page;
import fr.conceptit.imagelab.repository.PageRepository;
import fr.conceptit.imagelab.service.PageService;

@Service ("pageService")
@Repository
@Transactional
public class PageServiceImpl implements PageService {

	@Autowired
	private PageRepository pageRepository;
	
	
	@Override
	public List<Page> findAll() {
		return Lists.newArrayList(pageRepository.findAll());
	}

	@Override
	public Page findById(Long id) {
		return pageRepository.findOne(id);
	}

	@Override
	public Page save(Page page) {
		return pageRepository.save(page);
	}

}
