package fr.conceptit.imagelab.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import fr.conceptit.imagelab.domain.CImage;
import fr.conceptit.imagelab.repository.CImageRepository;
import fr.conceptit.imagelab.service.CImageService;


@Service("cimageService")
@Repository
@Transactional
public class CImageServiceImpl implements CImageService {

	
	@Autowired
	private CImageRepository cimageRepository;
	
	
	@Override
	@Transactional(readOnly=true)
	public List<CImage> findAll() {
		return Lists.newArrayList(cimageRepository.findAll());
	}

	@Override
	@Transactional(readOnly=true)
	public CImage findById(Long id) {
		return cimageRepository.findOne(id);
	}

	@Override
	public CImage save(CImage cimage) {
		return cimageRepository.save(cimage);
	}

	@Transactional(readOnly=true)
	@Override
	public Page<CImage> findAllByPage(Pageable pageable) {
		return cimageRepository.findAll(pageable);
	}

	
	
	
}
