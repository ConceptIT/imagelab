package fr.conceptit.imagelab.service;

import java.util.List;

import fr.conceptit.imagelab.domain.Page;

public interface PageService {

	public List<Page> findAll();

	public Page findById(Long id);

	public Page save(Page page);
	
}
