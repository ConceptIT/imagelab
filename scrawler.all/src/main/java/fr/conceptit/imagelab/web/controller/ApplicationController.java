package fr.conceptit.imagelab.web.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.conceptit.imagelab.domain.Application;
import fr.conceptit.imagelab.service.ApplicationService;
import fr.conceptit.imagelab.web.form.Message;
import fr.conceptit.imagelab.web.util.UrlUtil;

@RequestMapping("/applications")
@Controller
public class ApplicationController {

	final Logger logger = LoggerFactory.getLogger(ApplicationController.class);

	@Autowired
	private ApplicationService applicationService;

	@Autowired
	MessageSource messageSource;

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		logger.info("Listing applications");

		List<Application> applications = applicationService.findAll();
		uiModel.addAttribute("applications", applications);

		logger.info("No. of applications : " + applications.size());

		return "applications/list";
	}

	// Other code omitted
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") Long id, Model uiModel) {
		Application application = applicationService.findById(id);
		uiModel.addAttribute("application", application);
		return "applications/show";
	}

	// add new application
	@RequestMapping(params = "form", method = RequestMethod.POST)
	public String create(@Valid Application application, BindingResult bindingResult,
			Model uiModel, HttpServletRequest httpServletRequest,
			RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Creating new application");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute(
					"message",
					new Message("error", messageSource.getMessage(
							"application_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("application", application);
			return "applications/create";
		}

		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute(
				"message",
				new Message("success", messageSource.getMessage(
						"application_save_success", new Object[] {}, locale)));

		logger.info("Application id : " + application.getId());

		applicationService.save(application);

		return "redirect:/applications/"
				+ UrlUtil.encodeUrlPathSegment(application.getId().toString(),
						httpServletRequest);
	}

	@RequestMapping(params = "form", method = RequestMethod.GET)
	public String createForm(Model uiModel) {
		Application application = new Application();
		uiModel.addAttribute("application", application);
		return "applications/create";
	}

	// Update Form
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.POST)
	public String update(@Valid Application application, BindingResult bindingResult,
			Model uiModel, HttpServletRequest httpServletRequest,
			RedirectAttributes redirectAttributes, Locale locale) {

		logger.info("Updating application");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute(
					"message",
					new Message("error", messageSource.getMessage(
							"application_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("application", application);
			return "applications/update";
		}

		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute(
				"message",
				new Message("success", messageSource.getMessage(
						"application_save_success", new Object[] {}, locale)));
		applicationService.save(application);
		return "redirect:/applications/"
				+ UrlUtil.encodeUrlPathSegment(application.getId().toString(),
						httpServletRequest);
	}

	// Update Send
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Long id, Model uiModel) {
		uiModel.addAttribute("application", applicationService.findById(id));
		return "applications/update";
	}

}
