package fr.conceptit.imagelab.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.conceptit.imagelab.domain.Application;
import fr.conceptit.imagelab.domain.Crawler;
import fr.conceptit.imagelab.service.CrawlerService;

@RequestMapping("/crawlers")
@Controller
public class CrawlerController {

	final Logger logger = LoggerFactory.getLogger(CrawlerController.class);

	@Autowired
	private CrawlerService crawlerService;

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		logger.info("Listing crawlers");

		List<Crawler> crawlers = crawlerService.findAll();
		uiModel.addAttribute("crawlers", crawlers);

		logger.info("No. of crawlers: " + crawlers.size());

		return "crawlers/list";
	}

	// Other code omitted
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") Long id, Model uiModel) {
		Crawler crawler = crawlerService.findById(id);
		uiModel.addAttribute("crawler", crawler);
		return "crawlers/show";
	}

}
