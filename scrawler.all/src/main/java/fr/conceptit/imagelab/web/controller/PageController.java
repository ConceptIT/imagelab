package fr.conceptit.imagelab.web.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import fr.conceptit.imagelab.domain.Page;
import fr.conceptit.imagelab.service.PageService;
import fr.conceptit.imagelab.web.form.Message;
import fr.conceptit.imagelab.web.util.UrlUtil;

@Controller
@RequestMapping("/pages")
public class PageController {

	final Logger logger = LoggerFactory.getLogger(PageController.class);
	@Autowired
	private PageService pageService;

	@Autowired
	MessageSource messageSource;

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model uiModel) {
		logger.info("Listing pages");

		List<Page> pages = pageService.findAll();
		uiModel.addAttribute("pages", pages);

		logger.info("No. of pages: " + pages.size());

		return "pages/list";
	}

	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") Long id, Model uiModel) {
		Page page = pageService.findById(id);
		uiModel.addAttribute("page", page);
		return "pages/show";
	}

	// add new page
	@RequestMapping(params = "form", method = RequestMethod.POST)
	public String create(@Valid Page page, BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest,
			RedirectAttributes redirectAttributes, Locale locale) {
		logger.info("Creating new page");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute(
					"message",
					new Message("error", messageSource.getMessage(
							"page_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("page", page);
			return "pages/create";
		}

		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute(
				"message",
				new Message("success", messageSource.getMessage(
						"page_save_success", new Object[] {}, locale)));

		logger.info("page id : " + page.getId());

		pageService.save(page);

		return "redirect:/pages/"
				+ UrlUtil.encodeUrlPathSegment(page.getId().toString(),
						httpServletRequest);
	}

	@RequestMapping(params = "form", method = RequestMethod.GET)
	public String createForm(Model uiModel) {
		Page page = new Page();
		uiModel.addAttribute("page", page);
		return "pages/create";
	}

	// Update Form
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.POST)
	public String update(@Valid Page page, BindingResult bindingResult,
			Model uiModel, HttpServletRequest httpServletRequest,
			RedirectAttributes redirectAttributes, Locale locale) {

		logger.info("Updating page");
		if (bindingResult.hasErrors()) {
			uiModel.addAttribute(
					"message",
					new Message("error", messageSource.getMessage(
							"page_save_fail", new Object[] {}, locale)));
			uiModel.addAttribute("page", page);
			return "pages/update";
		}

		uiModel.asMap().clear();
		redirectAttributes.addFlashAttribute(
				"message",
				new Message("success", messageSource.getMessage(
						"page_save_success", new Object[] {}, locale)));
		pageService.save(page);
		return "redirect:/pages/"
				+ UrlUtil.encodeUrlPathSegment(page.getId().toString(),
						httpServletRequest);
	}

	// Update Send
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Long id, Model uiModel) {
		uiModel.addAttribute("page", pageService.findById(id));
		return "pages/update";
	}

}
