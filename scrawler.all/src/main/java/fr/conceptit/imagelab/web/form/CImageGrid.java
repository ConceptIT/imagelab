package fr.conceptit.imagelab.web.form;

import java.util.List;

import fr.conceptit.imagelab.domain.CImage;

public class CImageGrid {

	private int totalPages;
	private int currentPage;
	private long totalRecords;
	private List<CImage> cimageData;
	
	public List<CImage> getCimageData() {
		return cimageData;
	}
	public void setCimageData(List<CImage> cimageData) {
		this.cimageData = cimageData;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public long getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}

	
	
	
}
