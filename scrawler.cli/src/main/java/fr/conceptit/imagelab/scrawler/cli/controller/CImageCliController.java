package fr.conceptit.imagelab.scrawler.cli.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.conceptit.imagelab.cascadeclassifier.service.AdaptiveBoosting;
import fr.conceptit.imagelab.io.CImageReader;
import fr.conceptit.imagelab.io.CImageWriter;
import fr.conceptit.imagelab.scrawler.Constants;
import fr.conceptit.imagelab.scrawler.domain.CImage;
import fr.conceptit.imagelab.scrawler.domain.Classifier;
import fr.conceptit.imagelab.scrawler.service.CImageService;
import fr.conceptit.imagelab.scrawler.service.ClassifierService;

@Component
public class CImageCliController {

	final Logger logger = LoggerFactory.getLogger(CImageCliController.class);
	private Set<Classifier> classifiers = new HashSet<Classifier>();

	@Autowired
	private CImageService cimageService;

	@Autowired
	private ClassifierService classifierService;

	@Autowired
	private AdaptiveBoosting adaBoost;

	public List<CImage> list() {
		logger.info("Listing cimages");
		List<CImage> cimages = cimageService.findAll();
		logger.info("No. of cimages: " + cimages.size());
		return cimageService.findAll();
	}

	public void add(String imageFileName) throws MalformedURLException, IOException {
		logger.info("add image" + imageFileName);

		CImageReader cimgReader = new CImageReader(imageFileName, true);
		CImage cimg = new CImage();
		cimg.setUrl(Constants.DIR_IMAGES.concat(cimgReader.getFileName()));
		cimg.setCreateDate(new DateTime());
		cimg.setFileName(cimgReader.getFileName());
		cimg.setNameApplication("OutWit Image");
		cimg.setUrlPage(Constants.DIR_IMAGES.concat(cimgReader.getFileName()));
		cimg.setMd5(cimgReader.getMd5());

		Mat imgData = cimgReader.getImageAsMat();
		boolean toPersist = false;
		for (Classifier classifier : classifiers) {

			MatOfRect modelDetections = adaBoost.detect(classifier.getFileName(), imgData, true, classifier.getColor());
			if (modelDetections.toArray().length != 0) {
				cimg.getClassifiers().add(classifier);
				cimg.setTagClassifier(classifier.getTags());
				toPersist = true;
			}

			if (toPersist) {
				logger.debug("### Persisantce DB");
				cimageService.save(cimg);
				logger.debug("### Persisantce FileSystem ");
				CImageWriter cimgWriter = new CImageWriter(cimgReader.getFileName(), imgData);
				cimgWriter.save(cimgReader.getExtension());
			}
		}

	}

	public void setNameClassifiers(Set<String> nameClassifiers) {
		for (String name : nameClassifiers) {
			Classifier classifierInDataBase = classifierService.findById(Long.parseLong(name));
			if (classifierInDataBase != null)
				this.classifiers.add(classifierInDataBase);
		}
	}

}
