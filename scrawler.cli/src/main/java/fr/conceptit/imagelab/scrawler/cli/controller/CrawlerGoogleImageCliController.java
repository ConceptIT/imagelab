package fr.conceptit.imagelab.scrawler.cli.controller;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.conceptit.imagelab.scrawler.service.impl.CrawlerGoogleImage;

@Component
public class CrawlerGoogleImageCliController {
	
	final Logger logger = LoggerFactory.getLogger(CrawlerGoogleImageCliController.class);
	
	private int nbrMaxOfImages;
	private int responsePerPage;
	private Set<String> nameClassifiers = new HashSet<String>();
	private String query;

	@Autowired
	private CrawlerGoogleImage crawlerGoogleImage;	


	public void go() {
		
		crawlerGoogleImage.setResponsePerPage(responsePerPage);
		crawlerGoogleImage.setNbrMaxOfImages(nbrMaxOfImages);
		crawlerGoogleImage.setQuery(query);
		crawlerGoogleImage.setUrl("https://ajax.googleapis.com/ajax/services/search/images?v=1.0");
		crawlerGoogleImage.setNameClassifiers(nameClassifiers);
		crawlerGoogleImage.crawler();
	
	}

	public CrawlerGoogleImageCliController(){
		
	}
	
	public CrawlerGoogleImageCliController(String query, Set<String> nameClassifiers, int reponsePerPage, int nbrMaxOfImages) {
		try {
			if ( nameClassifiers.isEmpty() || responsePerPage < 0 ||  responsePerPage > 8 || query.isEmpty() || nbrMaxOfImages < 0) {
				throw new Exception("Impossible de lancer le crawler !");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		setQuery(query);
		setResponsePerPage(reponsePerPage);
		setNameClassifiers(nameClassifiers);
		setNbrMaxOfImages(nbrMaxOfImages);
	}

	
	public int getNbrMaxOfImages() {
		return nbrMaxOfImages;
	}

	public void setNbrMaxOfImages(int nbrMaxOfImages) {
		this.nbrMaxOfImages = nbrMaxOfImages;
	}

	public int getResponsePerPage() {
		return responsePerPage;
	}

	public Set<String> getNameClassifiers() {
		return nameClassifiers;
	}

	public void setResponsePerPage(int responsePerPage) {
		this.responsePerPage = responsePerPage;
	}

	public void setNameClassifiers(Set<String> nameClassifiers) {
		this.nameClassifiers = nameClassifiers;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	
}
