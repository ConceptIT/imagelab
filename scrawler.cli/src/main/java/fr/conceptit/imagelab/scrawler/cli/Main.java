package fr.conceptit.imagelab.scrawler.cli;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.opencv.core.Core;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.GenericXmlApplicationContext;

import fr.conceptit.imagelab.scrawler.cli.controller.CImageCliController;
import fr.conceptit.imagelab.scrawler.cli.controller.CrawlerGoogleImageCliController;
import fr.conceptit.imagelab.scrawler.cli.controller.SeleniumCrawlerWDBonCoinCliController;

public class Main {

	final static Logger logger = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) {

		String nameApplication = null;
		String query = null;
		String workDir;
		List<String> imagesInDir = new ArrayList<String>();
		Boolean fromOutWit = false, fromOurCrawler = false;

		System.load("/opt/jar/native/linux/x86_64/libopencv_java248.so");

		Options options = new Options();
		options.addOption("a", true, "Application name : googleimage");
		options.addOption("q", true, "Query for search. For example : adidas");
		options.addOption("d", true, "Get images from a given directory");
		CommandLineParser parser = new BasicParser();

		try {
			CommandLine cmd = parser.parse(options, args);

			if (cmd.hasOption("d")) {
				workDir = cmd.getOptionValue("d");
				fromOutWitDirToDB(workDir, imagesInDir);
				fromOutWit = true;

			} else if (cmd.hasOption("a") && cmd.hasOption("q")) {
				nameApplication = cmd.getOptionValue("a");
				query = cmd.getOptionValue("q");
				fromOurCrawler = true;
				
			} else {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("scrawler ", options);
				return;
			}

		} catch (ParseException exp) {
			// oops, something went wrong
			System.err.println("Parsing failed.  Reason: " + exp.getMessage());
		}

		GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
		ctx.load("classpath:app-context.xml");
		ctx.refresh();

		Set<String> nameClassifiers = new HashSet<String>(Arrays.asList("3", "4", "7", "8", "9"));

		if (fromOutWit) {
			CImageCliController cimageController = ctx.getBean(CImageCliController.class);
			cimageController.setNameClassifiers(nameClassifiers);

			for (String imageFile : imagesInDir) {
				try {
					cimageController.add(imageFile);
				} catch (MalformedURLException e) {
					continue;
				} catch (IOException e) {
					continue;
				}
			}
		}

		if (fromOurCrawler) {
			
			if (nameApplication.compareTo("googleimage") == 0) {
				CrawlerGoogleImageCliController crawlerGoogleController = ctx.getBean(CrawlerGoogleImageCliController.class);
				crawlerGoogleController.setNameClassifiers(nameClassifiers);
				crawlerGoogleController.setQuery(query);
				crawlerGoogleController.setNbrMaxOfImages(100);
				crawlerGoogleController.setResponsePerPage(8);
				crawlerGoogleController.go();
			}

			if (nameApplication.compareTo("boncoin") == 0) {
				
				SeleniumCrawlerWDBonCoinCliController crawlerBonCoinControler = ctx.getBean(SeleniumCrawlerWDBonCoinCliController.class);
				crawlerBonCoinControler.setNameClassifiers(nameClassifiers);
				crawlerBonCoinControler.setQuery(query);
				crawlerBonCoinControler.setNbrMaxOfImages(100);
				crawlerBonCoinControler.go();
				
			}
			
			
			
		}

	}

	public static void fromOutWitDirToDB(String workDirectory, List<String> listFiles) {
		File folder = new File(workDirectory);
		File[] listOfFiles = folder.listFiles( new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return ( name.endsWith("*.png") || name.endsWith("jpg") );
			}
		});
		for (File file : listOfFiles) {
			if (file.isFile() ) {
				listFiles.add(file.getAbsolutePath());
				logger.info(file.getAbsolutePath());
			}
		}
	}

}
