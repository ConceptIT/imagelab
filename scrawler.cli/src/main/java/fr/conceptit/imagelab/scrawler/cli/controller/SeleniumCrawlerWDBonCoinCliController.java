package fr.conceptit.imagelab.scrawler.cli.controller;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.conceptit.imagelab.scrawler.service.impl.SeleniumCrawlerWDBonCoin;

@Component
public class SeleniumCrawlerWDBonCoinCliController {

	final Logger logger = LoggerFactory.getLogger(SeleniumCrawlerWDBonCoinCliController.class);
	private int nbrMaxOfImages;
	private Set<String> nameClassifiers = new HashSet<String>();
	private String query;

	@Autowired
	private SeleniumCrawlerWDBonCoin crawlerSeleniumBonCoin;

	public void go() {

		crawlerSeleniumBonCoin.setNbrMaxOfImages(nbrMaxOfImages);
		crawlerSeleniumBonCoin.setQuery(query);
		crawlerSeleniumBonCoin.setUrl("http://www.leboncoin.fr/annonces/offres/ile_de_france/");
		crawlerSeleniumBonCoin.setNameClassifiers(nameClassifiers);
		crawlerSeleniumBonCoin.crawler();

	}

	public int getNbrMaxOfImages() {
		return nbrMaxOfImages;
	}

	public Set<String> getNameClassifiers() {
		return nameClassifiers;
	}

	public String getQuery() {
		return query;
	}

	public void setNbrMaxOfImages(int nbrMaxOfImages) {
		this.nbrMaxOfImages = nbrMaxOfImages;
	}

	public void setNameClassifiers(Set<String> nameClassifiers) {
		this.nameClassifiers = nameClassifiers;
	}

	public void setQuery(String query) {
		this.query = query;
	}

}
